window.onload = function() {
  var nav = document.getElementById("toggle-nav");
  if(nav) {
      nav.addEventListener('click', changeNavClass);
    }

  var closeNav = document.getElementById("close-nav");
  if(closeNav) {
      closeNav.addEventListener('click', changeNavClass);
    }

  function changeNavClass() {
      var container = document.getElementById("nav-container");
      container.className = container.className === "opened" ? "" : "opened";
    }
}

