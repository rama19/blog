if @image.errors.empty?
  json.image_url @image.url
else
  json.errors @image.errors.full_messages.join("; ")
end
