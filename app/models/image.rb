class Image < ActiveRecord::Base

  OPTIONS_FOR_URL = {
    thumb: {size: "190x190", crop: "thumb" }
  }

  has_attachment :attachment, accept: [:png, :jpg, :jpeg, :gif]

  validates :attachment, presence: true

  delegate :width, :height, to: :attachment

  def url(**options)
    Cloudinary::Utils.cloudinary_url(attachment.path, options)
  end

  def url_for(key)
    options = OPTIONS_FOR_URL.fetch(key)
    url(options)
  end
end
