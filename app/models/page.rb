class Page < ActiveRecord::Base
  has_attachment :image, accept: [:jpg, :png, :gif, :jpeg]

  validates :image, presence: true

  validates :title, presence: true
  validates :short_description, presence: true
  validates :body, presence: true
  validates :slug, presence: true

  scope :published, -> { where(published: true) }

  before_validation :generate_slug!

  private

  def generate_slug!
    return if slug.present?
    self.slug = title.parameterize
  end
end
