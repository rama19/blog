module Admin::PagesHelper
  PAGE_IMAGE_OPTIONS = {
    small: { width: 20, height: 20 },
    no_image: { width: 0, height: 0 },
    large: { width: 990, height: 400, class: "image" }
  }

  def page_image_tag(page, size)
    image_path = page_image_path(page, size)
    options = PAGE_IMAGE_OPTIONS[size]
    cl_image_tag(image_path, options)
  end

  def page_image_path(page, size)
    if page.image.present?
      page.image.path
    else
      nil
    end
  end

  def admin_menu_items
    [
      ["Home", admin_root_path],
      ["New article", new_admin_page_path],
      ["Blog", root_path]
    ]
  end

  def render_admin_menu
    items = admin_menu_items
    return "" if items.empty?
    lis = items.map do |item|
      li_class = current_page?(item[1]) ? "active" : nil
      content_tag(:li, link_to(item[0], item[1]), class: li_class)
    end
    content_tag(:ul, lis.join("").html_safe, class: "admin-nav pull-right")
  end

  def render_flash_messages
    msg = nil
    class_name = nil
    [:notice, :success, :alert].each do |key|
      next if flash[key].blank?
      msg = flash[key]
      class_name = key
      class_name = :success if %w(notice success).include?(key.to_s)
      break
    end
    return if msg.blank?
    close_link = link_to("x", "#", class: "close")
    msg.blank? ? "" : content_tag(:div, "#{close_link}#{msg}".html_safe, :class => "alert alert-#{class_name}", "data-dismiss" => "alert")
  end
end
