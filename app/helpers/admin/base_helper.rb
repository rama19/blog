module Admin::BaseHelper

  def human_name_for_boolean(boolean)
    boolean ? "YES" : "NO"
  end
end
