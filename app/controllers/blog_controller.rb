class BlogController < ApplicationController

  def home
    @pages = Page.all.order("created_at DESC").page(params[:page]).per_page(6)
  end

  def show
    @page = Page.find_by_slug(params[:slug])
  end

  def about

  end
end
