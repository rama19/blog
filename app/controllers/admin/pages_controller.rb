class Admin::PagesController < Admin::BaseController

  def index
    @pages = Page.all.order("created_at DESC")
  end

  def new
    @page = Page.new
  end

  def create
    @page = Page.new(page_params)

    if @page.save
      redirect_to admin_root_path, notice: "Article created!"
    else
      render 'new'
    end
  end

  def edit
    @page = Page.find(params[:id])
  end

  def update
    @page = Page.find(params[:id])

    if @page.update(page_params)
      redirect_to admin_root_path, notice: "Changes saved successfully!"
    else
      render 'edit'
    end
  end

  def destroy
    @page = Page.find(params[:id])
    @page.destroy

    redirect_to admin_root_path
  end


  private

  def page_params
    params.require(:page).permit(:title, :image, :short_description, :body, :slug, :published)
  end
end
