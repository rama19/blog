Rails.application.routes.draw do
  mount Attachinary::Engine => "/attachinary"

  root to: "blog#home"

  get "blog/:slug" => "blog#show", as: :show_page

  %w(about).each do |page|
    get page, to: "blog##{page}", as: page
  end
  namespace :admin do
    root to: "pages#index"

    resources :pages, only: [:create, :index, :new, :edit, :update, :destroy]
    resources :images, only: [:index, :create]
  end
end
