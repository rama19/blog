require 'test_helper'
require 'support'

class Admin::BaseControllerTest < ActionController::TestCase

  test "should get admin index" do
    http_login
    get :index
    assert_response :success
  end
end
