require 'test_helper'
require 'support'
include Support

class Admin::PagesControllerTest < ActionController::TestCase

  setup do
    @page = pages(:one)
  end

  test "should get admin index" do
    http_login
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create article" do
    assert_difference('Page.count') do
      post :create, page: { title: @page.title, short_description: @page.short_description, body: @page.body, slug: @page.slug }
    end

    assert_redirected_to :index
  end

  test "should get edit" do
    get :edit, id: @page
    assert_response :success
  end

  test "should update article" do
    patch :update, id: @page, page: { title: @page.title, short_description: @page.short_description, body: @page.body, slug: @page.slug}
    assert_redirected_to :index
  end

  test "should destroy article" do
    assert_difference('Page.count', -1) do
      delete :destroy, id: @page
    end
    assert_redirected_to :index
  end

end
