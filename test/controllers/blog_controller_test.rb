require 'test_helper'
require 'support'

class BlogControllerTest < ActionController::TestCase

  test "should get home page" do
    get :home
    assert_response :success
    assert_not_nil assigns(:pages)
  end

  test "should get about page" do
    get :about
    assert_response :success
  end
end
