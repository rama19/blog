require 'test_helper'

class PageTest < ActiveSupport::TestCase

  test "page with all attributes is valid" do
    page = create_page
    assert page.invalid?
  end

  test "page is invalid if title is missing" do
    page = create_page(title: nil)
    assert page.invalid?
  end

  test "page is invalid if short description is missing" do
    page = create_page(short_description: nil)
    assert page.invalid?
  end

  test "page is invalid if body is missing" do
    page = create_page(body: nil)
    assert page.invalid?
  end

  test "page has title attribute" do
    assert Page.new.respond_to?(:title)
  end

  test "page has short description attribute" do
    assert Page.new.respond_to?(:short_description)
  end

  test "page has body attribute" do
    assert Page.new.respond_to?(:body)
  end

  test "page has slug attribute" do
    assert Page.new.respond_to?(:slug)
  end

  private

  def create_page(attributes = {})
    default_attributes = {
      title: "First article title",
      short_description: "This is some short description",
      body: "This is article content",
      slug: "first-article-title"
    }
    Page.new(default_attributes.merge(attributes))
  end
end
