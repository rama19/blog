module Support

  def encoded_auth_credentials
    username = ENV.fetch["ADMIN_USERNAME"]
    password = ENV.fetch["ADMIN_PASSWORD"]

    ActionController::HttpAuthentication::Basic.encode_credentials(username, password)
  end

  def http_login
    request.env['HTTP_AUTHORIZATION'] = encoded_auth_credentials
  end

end

