require 'test_helper'

class UserFlowsTest < ActionDispatch::IntegrationTest

  test "login and create blog article" do
    visit('/admin')
    authenticate_admin!
      fill_in 'User Name', with: "admin"
      fill_in 'Password', with: "secret"
    redirect_to '/admin'
  end
end
