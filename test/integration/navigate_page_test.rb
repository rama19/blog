require 'test_helper'

class NavigatePageTest < ActionDispatch::IntegrationTest

  test "browse site" do
    get root_path
    assert_response :success
    click_link 'About'
    get about_path
    assert_response :success
  end

end
